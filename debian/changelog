angrydd (1.0.1-14) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.6.2.
  * Switch to debhelper-compat = 13.

 -- Markus Koschany <apo@debian.org>  Mon, 23 Jan 2023 21:15:23 +0100

angrydd (1.0.1-13) unstable; urgency=medium

  * Team upload.
  * Port to Python 3. (Closes: #912479)
  * Drop trailing whitespaces.
  * Point Vcs-* fields to salsa.
  * Drop upstream homepage and watch file as the URL no longer exists.
  * Update Standards-Version to 4.4.0:
    - declare that d/rules does not require root
  * Update to debhelper compat level 12.

 -- Reiner Herrmann <reiner@reiner-h.de>  Fri, 09 Aug 2019 20:47:59 +0200

angrydd (1.0.1-12) unstable; urgency=medium

  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.2.1.
  * Use https for Format field.
  * Drop deprecated menu file and xpm icon.
  * Drop source/lintian-overrides.
  * Use secure Homepage URI.

 -- Markus Koschany <apo@debian.org>  Sun, 28 Oct 2018 17:24:19 +0100

angrydd (1.0.1-11) unstable; urgency=medium

  * Declare compliance with Debian Policy 3.9.8.
  * Vcs fields: Use cgit and https.
  * Update my e-mail address.
  * wrap-and-sort -sa.
  * Use only Build-Depends field.
  * Use compat level 10.
  * debian/watch: Use version=4.

 -- Markus Koschany <apo@debian.org>  Sun, 24 Apr 2016 21:35:20 +0200

angrydd (1.0.1-10) unstable; urgency=medium

  * debian/menu: Add the absolute icon path. (Closes: #726205)
  * Add windowed-mode.patch.
    - Start Angry Drunken Dwarves in windowed mode by default.
  * Declare compliance with Debian Policy 3.9.5.
  * Update copyright years.

 -- Markus Koschany <apo@gambaru.de>  Sat, 18 Jan 2014 22:33:09 +0100

angrydd (1.0.1-9) unstable; urgency=low

  * New Maintainer. (Closes: #705148)
  * Switch to package format 3.0 (quilt).
  * Bump compat level to 9 and require debhelper >= 9.
  * Improve watch file by using a more flexible extension regex.
  * angrydd.desktop: Add keywords and a comment in German.
  * debian/control:
    - Drop cdbs, python-dev, and python-all-dev build dependencies. The game
      only requires a build dependency on python.
    - Drop also deprecated python-support build dependency and use dh_python2
      instead.
    - Move python build dependency to Build-Depends-Indep.
    - Bump Standards-Version to 3.9.4, no changes required.
    - Remove superfluous ${shlibs:Depends} variable.
    - Remove deprecated XB-Python-Version field.
    - Update Vcs-URIs. Angry Drunken Dwarves is maintained in a git
      repository from now on.
  * Remove deprecated pyversions file.
  * debian/rules:
    - Simplify debian/rules by using dh sequencer.
    - Build --with python2.
  * debian/patches: Prepare patches for quilt patch system. Rename all .diff
    extensions to .patch and create a series file.
  * Add DEP-3 headers to all patches.
  * Update debian/copyright to copyright format 1.0.
  * Install README and TODO via docs file.

 -- Markus Koschany <apo@gambaru.de>  Fri, 12 Apr 2013 19:17:18 +0200

angrydd (1.0.1-8) unstable; urgency=low

  * debian/patches/03_bug405368: Fixed use of tabs instead of spaces.
  * debian/patches/05_bug402333.diff: Fix traceback when a game is drawn.
    (Closes: #402333)

 -- Daniel Watkins <daniel@daniel-watkins.co.uk>  Sun, 19 Oct 2008 13:19:40 +0100

angrydd (1.0.1-7) unstable; urgency=low

  * Adopting. (Closes: #497239)
  * debian/control
    + Moved Homepage out of Description, as per lintian warning.
    + Bumped Standards-Version to 3.8.0, as per lintian warning.
    + Moved python-pygame from Build-Depends to Build-Depends-Indep, as per
      lintian warning.
    + Added Vcs-Bzr field.
  * Removed obsolete Encoding key from angrydd.desktop, as per lintian
    warning.

 -- Daniel Watkins <daniel@daniel-watkins.co.uk>  Sun, 31 Aug 2008 18:37:05 +0100

angrydd (1.0.1-6) unstable; urgency=low

  * debian/menu: Updated longtitle and section.  Closes: #445123.
  * debian/angrydd.desktop: Updated categories.

 -- Bart Martens <bartm@knars.be>  Wed, 03 Oct 2007 21:13:03 +0200

angrydd (1.0.1-5) unstable; urgency=low

  * debian/patches/03_bug405368.diff: Documented keys.  Closes: #405368.
  * debian/patches/04_bug406548.diff: Documented the fullscreen toggle in the
    manpage, and added this toggle to the config menu.  Closes: #406548.
  * debian/copyright: Updated.

 -- Bart Martens <bartm@knars.be>  Sat, 13 Jan 2007 18:37:16 +0100

angrydd (1.0.1-4) unstable; urgency=low

  * debian/patches/02_unixbros.diff: Breaking a crystal of "over 16 gems"
    unlocks "unixbros".  Closes: #403023.
  * debian/angrydd.xpm, debian/menu: Added menu icon and long title.

 -- Bart Martens <bartm@knars.be>  Fri, 15 Dec 2006 22:00:13 +0100

angrydd (1.0.1-3) unstable; urgency=low

  * debian/control: Architecture "all".  Closes: #400537.

 -- Bart Martens <bartm@knars.be>  Mon, 27 Nov 2006 07:50:51 +0100

angrydd (1.0.1-2) unstable; urgency=low

  * debian/copyright: Fixed.  Closes: #400237.

 -- Bart Martens <bartm@knars.be>  Fri, 24 Nov 2006 18:44:56 +0100

angrydd (1.0.1-1) unstable; urgency=low

  * Initial release.  Closes: #334173.
  * debian/pyversions: Needs python 2.3 or newer.
  * debian/control: Needs python-pygame (>= 1.6.2).
  * debian/patches/01_prefix_usr.diff: Patches PREFIX in Makefile.
  * debian/angrydd.desktop, debian/install: Added.
  * debian/watch: Added.

 -- Bart Martens <bartm@knars.be>  Mon, 13 Nov 2006 15:39:44 +0100
